from selenium import webdriver
from time import sleep
import pymongo
from pymongo import MongoClient
import requests
import schedule
import time
import datetime


class Player:
    def __init__(self):
        self.name = ''
        self.news = ''
        self.impact = ''
        self.source = ''
        self.date = ''

    def set_news(self):
        pass


class FantasyTool:
    def __init__(self):
        self.draftkings = 'https://www.draftkings.com'
        self.fanduel = 'https://www.fanduel.com'
        self.rotoworld = 'http://www.rotoworld.com/playernews/nba/basketball-player-news'
        self.browser = None
        self.dk_players = {}
        self.fd_players = {}

    def start_browser(self):
        """Method that initiates the browser and implicit wait time"""
        self.browser = webdriver.Chrome()
        self.browser.implicitly_wait(25)

    def fd_login(self):
        try:
            self.browser.get(self.fanduel)
            sign_in = self.browser.find_element_by_css_selector('#homepage > header > div > section.logged-out-user > '
                                                           'div.login-menu-block > a')
            sign_in.click()
            user_name = self.browser.find_element_by_name('email')
            pw = self.browser.find_element_by_name('password')
            user_name.send_keys('rgorham1@iona.edu')
            pw.send_keys('julie8610')
            sign_in_button = self.browser.find_element_by_css_selector('#ccf1 > div > div > '
                                                                       'div.login-card > div > input')
            sign_in_button.click()
        except Exception, e:
            print str(e)

    def get_fd_players(self):
        self.fd_login()
        rp_popup = self.browser.find_element_by_css_selector('body > div.games-body-wrap > '
                                                             'header > div > nav.secondary-nav > '
                                                             'div:nth-child(2) > div > button')
        rp_popup.click()
        lineups = self.browser.find_element_by_css_selector('body > div.games-body-wrap > header > div > nav.primary-nav > '
                                                       'ul > li:nth-child(2) > a > span')
        lineups.click()

        rosters = self.browser.find_elements_by_class_name('rosters')
        for line in rosters:
            roster = line.find_element_by_class_name('roster')
            pics = roster.find_elements_by_tag_name('img')
            for pic in pics:
                pic.click()

                name = self.browser.find_element_by_xpath('//*[@id="global-view"]/div/player-card/section/div[1]/'
                                                     'player-card-header/section/div[1]/div[2]/h2/div[1]/span')
                avg = self.browser.find_element_by_css_selector('#global-view > div > '
                                                                 'player-card > section > div.player-card__inner > '
                                                                 'player-card-header > section > '
                                                                 'div.player-card-header__stats-container > '
                                                                 'ul > li:nth-child(1) > dl > dd').text
                player_name = name.text
                today = datetime.date.today()
                self.fd_players[player_name] = [avg, 'FD', today]
                close = self.browser.find_element_by_css_selector('#global-view > div > player-card > '
                                                             'section > div.player-card__inner > button > i')
                close.click()
                sleep(1)

    def dk_login(self):
        try:
            self.browser.get(self.draftkings)
            sign_in = self.browser.find_element_by_css_selector('#template-body-wrapper > div.top-nav > a.sign-in-btn')
            sign_in.click()
            user_name = self.browser.find_element_by_name('Username')
            pw = self.browser.find_element_by_name('Password')
            user_name.send_keys('rgorham1@iona.edu')
            pw.send_keys('julie8610')
            sign_in_button = self.browser.find_element_by_css_selector('#signin-popup > div.login_section > '
                                                                  'div.modal-footer > a.dk-btn.dk-btn-success')
            sign_in_button.click()
        except Exception, e:
            print str(e)

    def get_dk_players(self):
        self.dk_login()
        lineups_link = self.browser.find_element_by_xpath('/html/body/header/div[1]/div/nav/ul[1]/li[2]/a')
        lineups_link.click()
        lineups = self.browser.find_elements_by_class_name('upcoming')
        for team, lineup in enumerate(lineups):
            if "MMA" not in lineup.text:
                tables = lineup.find_elements_by_tag_name('table')
                for table in tables:
                    rows = table.find_elements_by_tag_name('tr')
                    for num, row in enumerate(rows[1:9]):
                        link = row.find_element_by_tag_name('a')
                        link.click()
                        name = self.browser.find_element_by_css_selector('#player-info > div.player-info > h1').text
                        full_name = str(name).split()
                        full_name[0] = str(full_name[0]).capitalize()
                        full_name[1] = str(full_name[1]).capitalize()
                        name = ' '.join(full_name)
                        sleep(.5)
                        self.browser.execute_script("document.getElementById('fancybox-close').focus()")
                        x = self.browser.find_element_by_xpath('//*[@id="fancybox-close"]')
                        x.click()
                        sleep(.5)
                        row = str(row.text).split()
                        avg = row[3]
                        sleep(.5)

                        self.dk_players[name] = [avg, 'DK', team]

    def get_news(self, players, pages=2, dfs_check=False):
        self.browser.get(self.rotoworld)
        page = 0
        dfs_news = {}
        player_dict = {}
        while page < pages:
            player_news = self.browser.find_element_by_css_selector('#cp1_pnlNews > div > div.RW_pn')
            sleep(15)
            news_container = player_news.find_elements_by_class_name('pb')
            # TODO: change keywords to regex expressions
            keywords = ['DFS', 'boost', 'usage', 'ruled', '(', ')']
            for news in news_container:
                report = news.find_element_by_class_name('report').text
                impact = news.find_element_by_class_name('impact').text
                source = news.find_element_by_class_name('source').text
                date = news.find_element_by_class_name('date').text
                if players:
                    for player in players:
                        if player in news.text:
                            print 'Player affected is %s' % player
                            print 'here is the report: %s' % report
                            print 'here is the impact: %s' % impact.encode("utf-8")
                            print 'here is the source: %s' % source
                            print 'here is the date: %s' % date
                            player_dict[player] = {'Report': report, 'Analysis': impact, 'Source': source, 'Time': date}
                elif dfs_check:
                    for keyword in keywords:
                        if keyword in impact:
                            impact = impact.encode("utf-8")
                            dfs_news[keyword.upper()] = {'Report': report, 'Analysis': impact}

            next_page = self.browser.find_element_by_css_selector('#cp1_ctl00_btnNavigate1Bot')
            next_page.click()
            page += 1
            sleep(2)
            print 'checked page %s' % page
        if dfs_news:
            return dfs_news
        else:
            return player_dict

    @staticmethod
    def email(message):
        key = 'key-01577a41c5abaf547dfcdf0bc021c1f7'
        mailgun_domain = 'sandboxbf71083cbdc44a4c9ebcfd82601ba1d2.mailgun.org'
        url = 'https://api.mailgun.net/v3/{}/messages'.format(mailgun_domain)
        auth = ('api', key)
        receiver = 'rgorham1@iona.edu'

        data = {
            'from': 'Mailgun User <mailgun@{}>'.format(mailgun_domain),
            'to': receiver,
            'subject': 'DFS news',
            'text': str(message)
        }

        response = requests.post(url, auth=auth, data=data)
        response.raise_for_status()
        print 'email sent'

    def get_players(self, site):
        if site == 'fd':
            self.get_fd_players()
            players = self.fd_players
        else:
            self.get_dk_players()
            players = self.dk_players
        print '%s players are: ' % site
        for player in players:
            print player

    def job(self, pages, site, check_players=True, analysis=False):
        fd_players = ''
        if check_players:
            self.get_players(site)
        insight = self.get_news(fd_players, pages, analysis)
        # email(insight)

    # TODO: look into remote db mLab or heroku
    def save(self):
        try:
            client = MongoClient()
            print "Connected to db"
        except pymongo.errors.ConnectionFailure, e:
            print 'Connection failed: %s' % e
        db = client.players
        players = db.players
        for person, v in self.dk_players.iteritems():
            player = {'name': person, 'avg': float(v[0]), 'site': v[1], 'team': str(v[2])}
            players.insert(player)
        print 'Insertion complete'

ft = FantasyTool()
ft.start_browser()
ft.job(5, 'fd', False, True)
# TODO: check out fantasysp.com and nba.com stats

'''
def scheduler(task):
    schedule.every(9).minutes.do(task)
    while True:
        schedule.run_pending()
        time.sleep(1)
    print 'job done'


# job(7)
# scheduler(job)
'''
