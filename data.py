from openpyxl import Workbook, load_workbook
import csv
import re

# imports for merging into one xlsx
from pyexcel.cookbook import merge_all_to_a_book
from glob import glob
import argparse


class Data:
    def __init__(self):
        self.text_data = []
        self.wb = Workbook()
        self.csv = None
        self.row_data = {}
        self.output = None
        
    def read_text(self, filename):
        """
        Reads text file into text_data instance variable
        :param filename: file path of desired text file
            :type: str
        """
        with open(filename, "r") as text:
            for line in text:
                self.text_data.append(line.strip())
                
    def read_excel(self, filename):
        """
        Reads excel(xlsx) file and stores in wb instance var
        :param filename: file path of desired excel file
        """
        self.wb = load_workbook(filename)

    def save_excel(self):
        self.output = raw_input('Enter output filename: ')
        self.wb.save(self.output)
                
    def grab_col(self, filename=None, excel=False, csv_file=False):
        if csv_file:
            with open(filename, 'rb') as doc:
                reader =  csv.reader(doc)
                for row in self.csv:
                    print row[1]
        elif excel:
            ws = self.wb.active
            num_rows = ws.max_row
            for row in range(1, num_rows):
                cell = ws.cell(row=row, column=2).value
                print cell

    def grab_row(self, filename = None, excel=False, csv_file=False, condition=None):
        if csv_file:
            with open(filename, 'rb') as doc:
                reader =  csv.reader(doc)
                for row in self.csv:
                    print row
        if excel:
            ws = self.wb.active
            num_rows = ws.max_row
            for num, row in enumerate(ws.iter_rows()):
                if condition in [cell.value for cell in row]:
                    self.row_data[num] = [cell.value for cell in row]
            
    def create_xl(self, data):
        ws = self.wb.active
        
        for row in range(1,len(data)):
            for k, v in data.iteritems():
                print k
                ws.cell(row=row, column=1).value = data[k][0]
                ws.cell(row=row, column=2).value = k
                ws.cell(row=row, column=3).value = data[k][1]
        self.wb.save('test.xlsx')

    def create_csv(self, data):
        with open('output2.csv', 'wb') as output:
            writer = csv.writer(output, quoting=csv.QUOTE_ALL)
            
            for key, value in data.iteritems():
                session = '\'%s\'' % value[0]
                writer.writerow([key, session,value[1], value[2], value[3]])

    @staticmethod
    def merge_xl(directory, date_range = ''):
        """
        Grabs all file in directory and merges into one excel workbook
        """
        try:
            output = 'IHG Weekly VA Metrics 2016-%s.xlsx' % date_range
            # output = raw_input('Enter merged file name: ')
            merge_all_to_a_book(glob(directory), output)
        except NotImplementedError as e:
            print str(e)

        return output

    @staticmethod
    def merge_csv(directory):
        """
        Grabs all csvs in directory and merges into one excel workbook
        """

        # output = 'IHG Weekly VA  %s Metrics 2016-%s.xlsx' % self.range
        merge_all_to_a_book(glob(directory), output)

    @staticmethod
    def multi_sentence(user_input):
        return re.split("[?.!]", user_input, 1)[0]


t = raw_input('Enter file: ')
d = Data()
d.read_excel(t)
d.grab_row(excel=True, condition='Helpful')

for k, v in d.row_data.iteritems(): # find multiple sentences
    user_input = v[4]
    semantic = v[8]
    #user_input = user_input.split("?", 1)[0]
    user_input = d.multi_sentence(user_input)

    print user_input, semantic



